#!/bin/bash

snapList=()

snapAptRemoval="n"


# Checks if the script is run inside a terminal or not.
checkTerminal() {
  if ! tty -s ; then
    zenity --info --text="Please run this script from a terminal" # Will obviously fail if the zenity command doesn't exist.
    exit 0
  fi
}

checkRoot(){
  if [[ $EUID -ne 0 ]]; then
    echo "Please run this script as root"
    exit 1
  fi
}


checkSnapdInstall() {
  if ! command -v snap &> /dev/null
  then
    whiptail --title "Remove snap script" --msgbox "This script is intended for snap(s) and/or snapd removal, however it seems the snap command does not exist.. Exiting." 0 0
    exit 2
  fi
}


userWarning() {
  whiptail --title "Remove snap script" --yesno "The following snaps will be removed : \n$1\nAre you sure you want to continue?" 0 0

  # End script if answer no
  if [ "${PIPESTATUS[0]}" -ne "0" ]; then
      exit 3
  fi
}

askAptSnapRemoval() {
  if command -v apt &> /dev/null
  then
    whiptail --title "Remove snap program?" --yesno "We detected that you have access to the apt command, meaning you might be on debian based system.\nDo you want us to try to remove the snapd package and block it from reinstalling?" 0 0
    if [ "${PIPESTATUS[0]}" -ne "1" ]; then
      snapAptRemoval="y"
    fi
  fi
}

aptSnapRemoval() {
  if command -v apt &> /dev/null
  then
    apt autoremove snapd -y
    echo -e "Package: snapd\nPin: release *\nPin-Priority: -1" > /etc/apt/preferences.d/nosnap
  fi
}


checkTerminal
checkRoot
checkSnapdInstall


# Get all snaps in an array, remove the first member since it's not a snap
readarray -t snapList < <(snap list | sed 's/|/ /' | awk '{print $1}')
unset 'snapList[0]'
snapList=("${snapList[@]}")

# All snaps send to be read by the user
snapStrList=$(printf "%s\n" "${snapList[@]}")

# Ask the user only if they have snaps, else we do nothing
if [ ${#snapList} -ne 0 ]; then
  userWarning "$snapStrList"
fi

askAptSnapRemoval


cmpt=0
declare -a cannotBeRemoved

# While there is still snaps here, we try and remove if
while [ ${#snapList} -ne 0 ]
do
  if [ "${snapList[$cmpt]}" == "" ] || [ ${#snapList} -eq ${#cannotBeRemoved} ]; then
    break
  fi
  snap remove "${snapList[$cmpt]}"
  if [ "${PIPESTATUS[0]}" -ne "0" ]; then #Error case, snap cannot be removed
    cmpt=$((cmpt+1))
    cannotBeRemoved+=("${snapList[$cmpt]}")
  else
    unset snapList["$cmpt"]
    cannotBeRemoved=()
    snapList=("${snapList[@]}")
  fi
  if [ -z "${snapList[$cmpt]}" ]; then
    cmpt=0
  fi
done



if [ "$snapAptRemoval" = "y" ]; then
  aptSnapRemoval
fi




whiptail --title "Remove snap script" --msgbox "Finished operation, thanks for using this script !" 0 0
