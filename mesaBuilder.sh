#!/bin/bash

scriptName="Mesa builder script"

# Fetch current mesa version
ver=$(LANG=EN pacman -Qi mesa | grep -Po '^Version\s*: \K.+')

helpMsg(){
  echo -e "This script allows you to build and install the manjaro mesa packages with VAAPI & codecs on your system.\n
Usage : $0 <deleteChoice>
With :
  - <deleteChoice> : If you want the script to delete the mesa folders when done. 0 is yes, 1 is no"
}

# Checks if the script is run inside a terminal or not.
checkTerminal() {
  if ! tty -s ; then
    zenity --info --text="Please run this script from a terminal" # Will obviously fail if the zenity command doesn't exist.
    exit 0
  fi
}

checkRoot(){
  if [[ "$EUID" -eq 0 ]] ; then
    whiptailExit "Makepkg doesn't support root user.\nExiting." 1
  fi
}

# For exit messages
whiptailExit(){
  whiptail --title "$scriptName" --msgbox "$1" 0 0 || echo "$1"
  exit "$2"
}

# Manjaro is the only supported distro
checkOs(){
  # Fetch os-release informations into variables, ID is what we need
  id="$(. /etc/os-release && echo "$ID")"

  # Check if user is running manjaro, if not stop there
  if [ "$id" != "manjaro" ];
  then
    whiptailExit "This script is for Manjaro systems, which isn't what you're using.\nDetected $id instead\nExiting." 1
  fi
}

cloneRepo() {
  cd /tmp/ || whiptailExit "Encountered an error while going to the /tmp directory. Exiting" 2
  git clone "$1" "$2"
  cd "$2" || whiptailExit "Encountered an error while going to the $2 directory. Exiting" 3
}

# Tweaks the pkg build to compile and install mesa
mesaBuild(){
  # Rollback to installed mesa versiont
  git reset --hard "$(git log --grep="$ver" --pretty=format:"%H")"

  # Add codecs
  sed -i '/-D microsoft-clc=disabled/a \    -D video-codecs=vc1dec,h264dec,h264enc,h265dec,h265enc' PKGBUILD

  # Build and install
  makepkg -sir --noconfirm --skippgpcheck # WARNING : INGNORING THE GPG KEYS ISN'T RECOMMENDED OBVIOUSLY. Should you take that argument into another shell/script be ABSOLUTLY SURE it's safe to do so !
  cd /tmp/ || whiptailExit "Encountered an error while going to the /tmp directory post makepkg. Exiting" 4
}

mesa(){
  cloneRepo https://gitlab.manjaro.org/packages/extra/mesa.git mesa
  mesaBuild
}

mesa32(){
  cloneRepo https://gitlab.manjaro.org/packages/multilib/lib32-mesa.git mesa32
  mesaBuild
}

if [ "$1" = "-h" ]; then
  helpMsg
  exit 0
fi


checkTerminal
checkOs


checkRoot
if [ "$1" = "" ]; then
  whiptail --title "$scriptName" --defaultno --yesno "Do you want to remove the mesa folders when done?" 0 0
  delete=$?
else
  delete=$1
fi

if ! pacman -Q base-devel > /dev/null ; then
  echo 'Required package base-devel not installed. Installing...'
  sudo pacman -S base-devel
fi

mesa
mesa32
if [ "$delete" = "0" ]; then
  rm -Rf mesa
  rm -Rf mesa32
else
  echo 'You can find the mesa build folders in the /tmp folder.'
fi


echo "Done. Enjoy having VAAPI & codecs back !"
