#!/bin/bash

declare -a installArray
declare -a descriptionArray

descriptionCmd="null"
queryCmd="null"
installCmd="null"


# Checks if the script is run inside a terminal or not.
checkTerminal() {
  if ! tty -s ; then
    zenity --info --text="Please run this script from a terminal" # Will obviously fail if the zenity command doesn't exist.
    exit 0
  fi
}


checkRoot(){
  if [[ $EUID -ne 0 ]]; then
    echo "Please run this script as root"
    exit 1
  fi
}

checkOs(){
  # In every single case, we'll get the package list from the repositories, letting the users decide later what they want
  if command -v pacman -h &> /dev/null
  then
        installArray+=($(pacman -Ss pipewire | awk '/pipewire/{print}' | awk -F'/' '{print$2}' | awk -F' ' '{print$1}' | awk '/pipewire/{print}' | awk '{printf "%s ",$0} END {print ""}'))
        descriptionCmd="pacman -Si "
        queryCmd="pacman -Q "
        installCmd="pacman -S "
  else
      # shellcheck source=/etc/os-release
      source /etc/os-release
      whiptail --title "Pipewire Installer script" --msgbox "Sorry, this script does not support ${ID} for now !" 0 0
      exit 2
  fi

  cleanPackageArray
  populateDescriptionArray
}


populateDescriptionArray() {
  # I hope there's a better way to do this because this is not optimized at all for every choice.
  if command -v pacman -h &> /dev/null
  then
        for i in "${!installArray[@]}"; do
          descriptionArray+=("$(LANG=en $descriptionCmd "${installArray["$i"]}" | awk '/Description/{print}' | awk -F': ' '{print$2}')")
        done
  else
      whiptail --title "Pipewire Installer script" --msgbox "HOW DID YOU EVEN GET HERE?!" 0 0
      exit 2
  fi
}

whiptailArraySetup(){
  for i in "${!installArray[@]}"; do
    new_array+=( "${installArray[i]}" )
    new_array+=( "${descriptionArray[i]}" )
    if [[ "${descriptionArray[i]}" =~ "doc" ]] || [[ "${descriptionArray[i]}" =~ "legacy" ]]; then
      new_array+=( off )
    else
      new_array+=( on )
    fi
  done
  installArray=("${new_array[@]}")
  unset new_array
}


fixArray(){
  for i in "${!installArray[@]}"; do
    new_array+=( "${installArray[i]}" )
  done
  installArray=("${new_array[@]}")
  unset new_array
}

# Removes a package from the array if it's already installed
cleanPackageArray() {
  for i in "${!installArray[@]}"; do
    if $queryCmd "${installArray["$i"]}" &> /dev/null
    then
      unset installArray["$i"]
    fi
  done
  fixArray
}

pkgInstall() {
  $installCmd "${installArray[@]}"
}



checkTerminal
checkRoot
checkOs

whiptailArraySetup

if [ ${#installArray} -eq 0 ]; then
  whiptail --title "Pipewire Installer script" --msgbox "Seems like you don't have any package to install.. Or maybe you've selected nothing?\nExiting." 0 0
  exit 3
fi

whiptail --title "Pipewire Installer script" --msgbox "This script will install pipewire on your system !" 0 0
whiptail --title "WARNING" --yesno "THIS SCRIPT ONLY INSTALLS PIPEWIRE ON YOUR SYSTEM, MEANING THAT YOUR CURRENT SOUND SERVER WILL BE REMOVED !\nI AM NOT RESPONSIBLE IN ANY WAY IF YOUR SYSTEM BREAKS, THAT MEANS IF YOU THINK SOMETHING WILL BREAK, DON'T USE THIS SCRIPT !\nAre you sure you want to continue?" 0 0


# End script if answer no
if [ "${PIPESTATUS[0]}" -ne "0" ]; then
    exit 4
fi


# Packages to install
readarray -t installArray < <(whiptail --title "Pipewire Installer script" --checklist "Please select every single package you need (Use space to select/unselect them)" 0 0 10 "${installArray[@]}" 2>&1 > /dev/tty | sed 's/"//g')

cleanPackageArray

if [ ${#installArray} -eq 0 ]; then
  whiptail --title "Pipewire Installer script" --msgbox "Seems like the selected packages are already installed.. Exiting." 0 0
  exit 3
fi


echo "If your package manager tells you there's some packages conflicts, either remove them now if it allows you to do so, or remove them and restart the script"
pkgInstall


whiptail --title "Pipewire Installer script" --msgbox "Finished operation, thanks for using this script !" 0 0
