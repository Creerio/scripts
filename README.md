# Scripts

## Description

This repo contains scripts that may be of use to some people, maybe you?

Be warned however that they might not work in every single case.


## Scripts

  - **mesaBuilder** : Builds and installs mesa w/ hardware encoding/decoding on Manjaro systems (recommanded if you have an AMD gpu and don't want to switch to the AMDPRO drivers). Uses the system's mesa version to avoid breaking the system.

  - **pipewireInstall** : Installs pipewire on the system.

  - **removeSnaps** : Interactive script to remove every single snap on the system. It's also able on apt system to block snapd from installing

## How to use


**1.** Clone this repo. One way to do that is by using the clone button the url of this repo or right clicking on [this link](https://gitlab.com/Creerio/scripts.git) and copying it. Then in a terminal, use the following command : `git clone <url>`

**2.** Go inside the cloned folder. You can do that with `cd scripts/`

**3.** Run any script you'd like. This is done using `./<any script>` in a terminal
